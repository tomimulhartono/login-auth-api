<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Auth extends REST_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->model('Adhivasindo_model');
		
    }

    public function login()
    {
        $error = [];

        if(!$this->getPost('username')) {

			$error[] = 'Username harus diisi';
			
		}

        if(!$this->getPost('password')) {

			$error[] = 'Password harus diisi';
			
		} 

        if(count($error) > 0) {

            $this->response(['success' => false, 'message' => $error[0] ], 422);
			
        }

		$exist = $this->Adhivasindo_model->get_login($this->getPost('username'));
	
        if($exist) {

			$mpass = md5($this->getPost('password'));

			$data = $this->Adhivasindo_model->get_login_data($this->getPost('username'), $mpass);

			if($data) {

				$this->response( ['success'=> true, 'data' => $data]);

			} else {

				$this->response( ['success'=> false, 'message' => 'Password tidak sesuai!' ] );
			}

        } else {

            $this->response( ['success'=> false, 'message' => 'Username tidak ditemukan!' ], 404 );

        }

    }
	
}