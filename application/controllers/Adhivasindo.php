<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adhivasindo extends CI_Controller {

	public function __construct() {

        parent::__construct();

    }

    public function getNameFromApi() {

        $api_url = 'https://ogienurdiana.com/career/ecc694ce4e7f6e45a5a7912cde9fe131';

        $api_response = file_get_contents($api_url);

        $data = json_decode($api_response, true);

        if ($data['RC'] == 200) {

            $dataContent = $data['DATA'];

            $dataRows = explode("\n", $dataContent);

            foreach ($dataRows as $row) {

                $rowData = explode("|", $row);

                $nama = $rowData[0];
                $ymd = $rowData[1];
                $nim = $rowData[2];

                // Cek apakah NAMA adalah "Turner Mia"
                if ($nama == "Turner Mia") {

                    echo 'Nama: ' . $nama . '<br>';

                    echo 'YMD: ' . $ymd . '<br>';

                    echo 'NIM: ' . $nim . '<br>';

                    break;

                }

            }
			
        } else {
			
            echo 'Error: ' . $data['RCM'];

        }
    }

	public function getNimFromApi() {

        $api_url = 'https://ogienurdiana.com/career/ecc694ce4e7f6e45a5a7912cde9fe131';

        $api_response = file_get_contents($api_url);

        $data = json_decode($api_response, true);

        if ($data['RC'] == 200) {

            $dataContent = $data['DATA'];

            $dataRows = explode("\n", $dataContent);

            foreach ($dataRows as $row) {

                $rowData = explode("|", $row);

                $nama = $rowData[0];
                $ymd = $rowData[1];
                $nim = $rowData[2];

                // Cek apakah NIM adalah "9352078461"
                if ($nim == "9352078461") {

                    echo 'NIM: ' . $nim . '<br>';

                    echo 'Nama: ' . $nama . '<br>';

                    echo 'YMD: ' . $ymd . '<br>';

                    break;

                }

            }
			
        } else {
			
            echo 'Error: ' . $data['RCM'];

        }
    }

	public function getYmdFromApi() {

        $api_url = 'https://ogienurdiana.com/career/ecc694ce4e7f6e45a5a7912cde9fe131';

        $api_response = file_get_contents($api_url);

        $data = json_decode($api_response, true);

        if ($data['RC'] == 200) {

            $dataContent = $data['DATA'];

            $dataRows = explode("\n", $dataContent);

            foreach ($dataRows as $row) {

                $rowData = explode("|", $row);

                $nama = $rowData[0];
                $ymd = $rowData[1];
                $nim = $rowData[2];

                // Cek apakah YMD adalah "20230405"
                if ($ymd == "20230405") {

                    echo 'YMD: ' . $ymd . '<br>';

                    echo 'Nama: ' . $nama . '<br>';

                    echo 'NIM: ' . $nim . '<br>';

                    break;

                }

            }
			
        } else {
			
            echo 'Error: ' . $data['RCM'];

        }
    }

}