<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set("Asia/Jakarta");

class Users extends REST_Controller {

	public function __construct() {

        parent::__construct();

        $this->load->model('Adhivasindo_model');
		
    }

    public function index() {

		$data = $this->Adhivasindo_model->get_users();

        $this->response(['success' => true, 'data' => $data]);
		
    }

    public function show($id) {

		$data = $this->Adhivasindo_model->get_users_byuserid($id);

        $this->response(['success' => true, 'data' => $data]);

    }

    public function save() {
		
        $error = [];
		
        if(!$this->getPost('username')) {
			
			$error[] = 'Username harus diisi'; 
		
		}

        if(!$this->getPost('password')) { 
			
			$error[] = 'Password harus diisi'; 
		
		}

        if(!$this->getPost('address')) {

			$error[] = 'Alamat harus diisi';
		
		}
		
        if(!$this->getPost('phone')) {

			$error[] = 'Phone harus diisi';
		
		}

        if(count($error) > 0) {

            $this->response(['success' => false, 'message' => $error[0] ], 422);
			
        }

        $insert = [

            'username' => $this->getPost('username'),

            'password' => md5($this->getPost('password')),

            'address' => $this->getPost('address'),

            'phone' => $this->getPost('phone'),

            'created_at' => date('Y-m-d H:i:s')
			
        ];

		$exist = $this->Adhivasindo_model->get_login($this->getPost('username'));
		
		if($exist) {

			$this->response(['success' => true, 'message' => 'Username sudah tersedia, mohon gunakan lainnya!']);

		} else {

			$this->db->insert('t_users', $insert);

			$this->response(['success' => true, 'message' => 'User baru berhasil ditambahkan!']);

		}
		
    }

    public function update($id)
    {
        $update = [];

        if($this->getPost('password') ) {
			
			$update['password'] = md5($this->getPost('password'));

		}

		if($this->getPost('name') ) { 

			$update['name'] = $this->getPost('name'); 

		}

        if($this->getPost('address') ) { 

			$update['address'] = $this->getPost('address'); 

		}

        if($this->getPost('phone') ) {

			$update['phone'] = $this->getPost('phone'); 
			
		}

		$update['updated_at'] = date('Y-m-d H:i:s'); 
        
        $this->db->update('t_users', $update, ['id' => $id]);

        $this->response(['success' => true, 'message' => 'Berhasil merubah user!']);
    }
    
    public function delete($id)
    {

		$update['deleted_at'] = date('Y-m-d H:i:s'); 

        $this->db->update('t_users', $update, ['id' => $id]);

        $this->response(['success' => true, 'message' => 'Berhasil menghapus user!']);
    }

}