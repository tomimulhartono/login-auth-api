<?php
date_default_timezone_set("Asia/Jakarta");

class Adhivasindo_model extends CI_Model {
	
	function get_login($username) {

    	$sql = "SELECT username FROM t_users where username = ?";
		
		$query = $this->db->query($sql,array($username));

		return $query->row_array();
		
    }

	function get_login_data($username, $mpass) {

		$sql = "select * from t_users where username = ? and password = ?";
		
		$query = $this->db->query($sql,array($username, $mpass));

		return $query->row_array();

	}

	function get_users() {

		$sql = "SELECT * FROM t_users WHERE deleted_at IS NULL";
		
		$query = $this->db->query($sql);

		return $query->result_array();

	}

	function get_users_byuserid($id) {

		$sql = "SELECT * FROM t_users WHERE id = ?";
		
		$query = $this->db->query($sql, array($id));

		return $query->result_array();

	}

}